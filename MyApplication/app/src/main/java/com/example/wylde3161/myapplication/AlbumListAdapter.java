package com.example.wylde3161.myapplication;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.List;

/**
 * Created by Miri on 12/12/2016.
 */

public class AlbumListAdapter extends RecyclerView.Adapter<AlbumListAdapter.ViewHolder>{


    private static final String TAG = "DavidLog";
    private Context context;
    private List<AlbumInfo> albumsList;

    public AlbumListAdapter(Context context, List<AlbumInfo> list) {
        Log.i(TAG, "in C'tor AlbumListAdapter");
        this.context = context;
        this.albumsList = list;
    }

    @Override
    public AlbumListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.i(TAG, "onCreateViewHolder");
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.album_list_line,parent,false);
        return new AlbumListAdapter.ViewHolder(view);

        //TextView tv = new TextView(context);
        //return new AlbumListAdapter.ViewHolder(tv);
    }

    @Override
    public void onBindViewHolder(AlbumListAdapter.ViewHolder holder, final int position) {
        Log.i(TAG, "onBindViewHolder");
        holder.textView.setText(albumsList.get(position).name);
        holder.textView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(context, MainActivity.class);
                intent.putExtra("uid", albumsList.get(position).uid);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return albumsList.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        TextView textView;


        public ViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.textView_albums_line);
        }
    }




}
