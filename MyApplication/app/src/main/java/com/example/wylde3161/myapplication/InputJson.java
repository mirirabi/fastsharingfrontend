package com.example.wylde3161.myapplication;

import java.util.Dictionary;

/**
 * Created by Miri on 12/24/2016.
 */

public class InputJson {
    private Dictionary<Integer, PersonInfo> personalInfoMap;
    private Dictionary<Integer, AlbumSuggestions> albums;

    /**
     * NO args C'tor
     */
    public InputJson(){

    }

    /**
     * args C'tor
     */
    public InputJson(Dictionary<Integer, PersonInfo> pIDict, Dictionary<Integer, AlbumSuggestions> aSDict){
        personalInfoMap = pIDict;
        albums = aSDict;
    }
}
