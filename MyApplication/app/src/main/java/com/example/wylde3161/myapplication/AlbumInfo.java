package com.example.wylde3161.myapplication;

/**
 * Created by Miri on 12/12/2016.
 */

public class AlbumInfo {
    public String uid;
    public String name;

    public AlbumInfo(String myUid, String myName){
        uid = myUid;
        name = myName;
    }

    @Override
    public String toString() {
        return name;
    }
}
