package com.example.wylde3161.myapplication;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Layout;
import android.view.*;
import android.util.Log;
import android.widget.*;
import android.widget.LinearLayout.LayoutParams;
import java.util.*;
import java.net.*;
import java.lang.*;
import android.os.AsyncTask;
import java.io.*;
import android.net.*;
/*
import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.AppIndex;
import com.google.android.gms.appindexing.Thing;
import com.google.android.gms.common.api.GoogleApiClient;
*/
import com.google.gson.Gson;

import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;


public class MainActivity extends AppCompatActivity {

    private static final String TAG = "DavidLog";
    //private  List<String> list = new ArrayList<String>();
    //private RecyclerView albumsRecyclerView;
    //private RecyclerView.Adapter albumAdapter;
    //private RecyclerView.LayoutManager albumsLayoutManager;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private RecyclerView suggestRecyclerView;
    private RecyclerView.Adapter suggestAdapter;
    private RecyclerView.LayoutManager suggestLayoutManager;



    private ArrayAdapter<String> listAdapter;
    //private ListView listView;


    private TextView textView;
    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    //private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //ListView lv = (ListView) findViewById(R.id.LV);
        //listView = (ListView) findViewById(R.id.LV);
        mRecyclerView = (RecyclerView) findViewById(R.id.rv);
        suggestRecyclerView = (RecyclerView) findViewById(R.id.rvSeggestion);
        //albumsRecyclerView = (RecyclerView) findViewById(R.id.rv2);


        String stringUrl = "http://fastsharingsfly.azurewebsites.net/miri.aspx";
        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            new DownloadWebpageTask().execute(stringUrl);
        } else {
            textView.setText("No network connection available.");
        }


        mLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
        mRecyclerView.setLayoutManager(mLayoutManager);

        suggestLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        suggestRecyclerView.setLayoutManager(suggestLayoutManager);
        //albumsLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        //albumsRecyclerView.setLayoutManager(albumsLayoutManager);

    }

/*


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Start content download", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        Log.i(TAG, "onCreate");
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        //client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }
/*
    public void showSuggestions(View view) {
        Intent intent = new Intent(this, AlbumListActivity.class);
        intent.putExtra("", message);
    }
*/

    @Override
    protected void onStart() {
        super.onStart();
        Log.i(TAG, "onStart");
        }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i(TAG, "onResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i(TAG, "onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.i(TAG, "onStop");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i(TAG, "onRestart");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.i(TAG, "onSaveInstanceState");
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.i(TAG, "onRestoreInstanceState");

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    private class DownloadWebpageTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            // params comes from the execute() call: params[0] is the url.
            try {
                return downloadUrl(urls[0]);
            } catch (IOException e) {
                return "Unable to retrieve web page. URL may be invalid.";
            }
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            Log.d(TAG, "Started onPostExecute");
            if (result != null) {
                try {
                    //Gson gson = new Gson();
                    //InputJson inputJsonObj = new InputJson();
                    //inputJsonObj = gson.fromJson(result, InputJson.class);

                    Intent intent = getIntent();
                    String chosenAlbumUid = intent.getStringExtra("uid");
                    List<PersonInfo> people = new ArrayList<PersonInfo>();
                    List<PersonInfo> extraSuggestions = new ArrayList<PersonInfo>();
                    //List<AlbumInfo> albums = new ArrayList<AlbumInfo>();
                    List<String> albums = new ArrayList<String>();

                    JSONObject root = new JSONObject(result);
                    JSONObject pInfoJObj = root.getJSONObject("personalInfoMap");
                    JSONObject albumsJObj = root.getJSONObject("albums");
                    JSONObject chosenAlbumInfoJobj = albumsJObj.getJSONObject(chosenAlbumUid);

                    Iterator<String> iterator = pInfoJObj.keys();

                    while (iterator.hasNext()) {
                        String key = iterator.next();
                        JSONObject personInfo = pInfoJObj.getJSONObject(key);
                        PersonInfo p = new PersonInfo(personInfo.getString("name"),personInfo.getString("email"),personInfo.getString("imageUrl"));
                        extraSuggestions.add(p);
                    }

                    JSONObject albumsSuggestionsJObj = chosenAlbumInfoJobj.getJSONObject("suggestions");

                    Iterator<String> albumSugIt = albumsSuggestionsJObj.keys();
                    while (albumSugIt.hasNext()) {
                        String suggestedUidKey = albumSugIt.next();
                        Integer sugPersonScore = albumsSuggestionsJObj.getInt(suggestedUidKey);  //story.getJSONObject(suggestedUidKey);
                        SuggestedPerson sggestion = new SuggestedPerson(suggestedUidKey, sugPersonScore);
                        JSONObject pToAdd = pInfoJObj.getJSONObject(sggestion.personUid);
                        PersonInfo p = new PersonInfo(pToAdd.getString("name"), pToAdd.getString("email"), pToAdd.getString("imageUrl"));
                        people.add(p);
                    }

                    mAdapter = new FaceAdapter(MainActivity.this, people);
                    mRecyclerView.setAdapter(mAdapter);

                    suggestAdapter = new SuggestionAdapter(MainActivity.this, extraSuggestions);
                    suggestRecyclerView.setAdapter(suggestAdapter);

                    //albumAdapter = new AlbumListAdapter(MainActivity.this, albums);
                    //albumsRecyclerView.setAdapter(albumAdapter);

                    //listAdapter = new ArrayAdapter<String>(MainActivity.this, R.layout.album_list_line2, albums);
                    //listView.setAdapter(listAdapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.d(TAG, result);
            }
        }

        private String downloadUrl(String myurl) throws IOException {
            Log.d(TAG, "Started downloadUrl");
            InputStream is = null;
            // Only display the first 500 characters of the retrieved
            // web page content.
            int len = 38242;

            try {
                URL url = new URL(myurl);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                // Starts the query
                conn.connect();
                int response = conn.getResponseCode();
                Log.d(TAG, "The response is: " + response);
                is = conn.getInputStream();

                // Convert the InputStream into a string
                String contentAsString = readIt(is, len);
                return contentAsString;

                // Makes sure that the InputStream is closed after the app is
                // finished using it.
            } finally {
                if (is != null) {
                    is.close();
                }
            }
        }

        // Reads an InputStream and converts it to a String.
        public String readIt(InputStream stream, int len) throws IOException, UnsupportedEncodingException {
            Log.d(TAG, "Started readIt");
            Reader reader = null;
            String theString = IOUtils.toString(stream, "UTF-8");
            reader = new InputStreamReader(stream, "UTF-8");
            char[] buffer = new char[len];
            reader.read(buffer);
            Log.d(TAG, "Finished readIt");
            return theString;
            //return new String(buffer);
        }
    }
}
