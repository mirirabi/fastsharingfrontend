package com.example.wylde3161.myapplication;

public class PersonInfo {
     String name;
     String eMail;
     String imgUrl;

    public PersonInfo(){
    }

    public PersonInfo(String name, String eMail, String imgUrl)
    {
        this.name = name;
        this.eMail = eMail;
        this.imgUrl = imgUrl;
    }
}
