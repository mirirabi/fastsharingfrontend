package com.example.wylde3161.myapplication;

        import android.content.Context;
        import android.support.v7.widget.RecyclerView;
        import android.util.Log;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.TextView;
        import com.bumptech.glide.Glide;
        import android.widget.ImageView;
        import java.util.List;

/**
 * Created by wylde on 12/8/2016.
 */

public class SuggestionAdapter extends RecyclerView.Adapter<SuggestionAdapter.ViewHolder> {

    private static final String TAG = "DavidLog";
    private Context context;
    private List<PersonInfo> list;

    public SuggestionAdapter(Context context, List<PersonInfo> list) {
        Log.i(TAG, "in C'tor PersonInfo");
        this.context = context;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.i(TAG, "onCreateViewHolder");
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.item_seggestion_text,parent,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Log.i(TAG, "onBindViewHolder SuggestionAdapter");
        Glide.with(context).load(list.get(position).imgUrl).into(holder.imageView);
        holder.textView.setText(list.get(position).name);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        TextView textView;
        ImageView imageView;



        public ViewHolder(View itemView) {
            super(itemView);
            textView = (TextView) itemView.findViewById(R.id.suggestionTextView);
            imageView = (ImageView) itemView.findViewById(R.id.CircleView_suggest_image);
        }
    }
}
