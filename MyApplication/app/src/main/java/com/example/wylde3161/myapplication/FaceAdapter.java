package com.example.wylde3161.myapplication;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.util.List;

/**
 * Created by wylde on 12/8/2016.
 */

public class FaceAdapter extends RecyclerView.Adapter<FaceAdapter.ViewHolder> {

    private static final String TAG = "DavidLog";
    private Context context;
    private List<PersonInfo> list;

    public FaceAdapter(Context context, List<PersonInfo> list) {
        Log.i(TAG, "in C'tor FaceAdapter");
        this.context = context;
        this.list = list;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Log.i(TAG, "onCreateViewHolder");
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.item_profile_image,parent,false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Log.i(TAG, "onBindViewHolder");
        Glide.with(context).load(list.get(position).imgUrl).into(holder.imageView);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        ImageView imageView;

        public ViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.CircleView_profile_image);
        }
    }
}
