package com.example.wylde3161.myapplication;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.apache.commons.io.IOUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class AlbumListActivity extends AppCompatActivity {

    private static final String TAG = "DavidLog";
    private RecyclerView albumsRecyclerView;
    private RecyclerView.Adapter albumAdapter;
    private RecyclerView.LayoutManager albumsLayoutManager;
    private TextView textView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        albumsRecyclerView = (RecyclerView) findViewById(R.id.rv_album_list);

        String stringUrl = "http://fastsharingsfly.azurewebsites.net/miri.aspx";
        ConnectivityManager connMgr = (ConnectivityManager)
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected()) {
            new AlbumListActivity.DownloadWebpageTask().execute(stringUrl);
        } else {
            textView.setText("No network connection available.");
        }

        albumsLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        albumsRecyclerView.setLayoutManager(albumsLayoutManager);


        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }


    private class DownloadWebpageTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... urls) {

            // params comes from the execute() call: params[0] is the url.
            try {
                return downloadUrl(urls[0]);
            } catch (IOException e) {
                return "Unable to retrieve web page. URL may be invalid.";
            }
        }

        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(String result) {
            Log.d(TAG, "Started onPostExecute");
            if (result != null) {
                try {
                    List<AlbumInfo> albums = new ArrayList<AlbumInfo>();
                    JSONObject root = new JSONObject(result);
                    JSONObject pInfoJObj = root.getJSONObject("personalInfoMap");
                    JSONObject albumsJObj = root.getJSONObject("albums");
                    Iterator<String> iterator = pInfoJObj.keys();

                    Iterator<String> albumsIt = albumsJObj.keys();
                    while (albumsIt.hasNext()) {
                        String albumUidKey = albumsIt.next();
                        JSONObject albumInfoJObj = albumsJObj.getJSONObject(albumUidKey);
                        albums.add(new AlbumInfo(albumUidKey, albumInfoJObj.getString("albumName")));

                    }

                    albumAdapter = new AlbumListAdapter(AlbumListActivity.this, albums);
                    albumsRecyclerView.setAdapter(albumAdapter);


                } catch (JSONException e) {
                    e.printStackTrace();
                }
                Log.d(TAG, result);
            }
        }

        private String downloadUrl(String myurl) throws IOException {
            Log.d(TAG, "Started downloadUrl");
            InputStream is = null;
            // Only display the first 500 characters of the retrieved
            // web page content.
            int len = 38242;

            try {
                URL url = new URL(myurl);
                HttpURLConnection conn = (HttpURLConnection) url.openConnection();
                conn.setReadTimeout(10000 /* milliseconds */);
                conn.setConnectTimeout(15000 /* milliseconds */);
                conn.setRequestMethod("GET");
                conn.setDoInput(true);
                // Starts the query
                conn.connect();
                int response = conn.getResponseCode();
                Log.d(TAG, "The response is: " + response);
                is = conn.getInputStream();

                // Convert the InputStream into a string
                String contentAsString = readIt(is, len);
                return contentAsString;

                // Makes sure that the InputStream is closed after the app is
                // finished using it.
            } finally {
                if (is != null) {
                    is.close();
                }
            }
        }

        // Reads an InputStream and converts it to a String.
        public String readIt(InputStream stream, int len) throws IOException, UnsupportedEncodingException {
            Log.d(TAG, "Started readIt");
            Reader reader = null;
            String theString = IOUtils.toString(stream, "UTF-8");
            reader = new InputStreamReader(stream, "UTF-8");
            char[] buffer = new char[len];
            reader.read(buffer);
            Log.d(TAG, "Finished readIt");
            return theString;
            //return new String(buffer);
        }
    }

}
