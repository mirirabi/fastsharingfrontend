package com.example.wylde3161.myapplication;

/**
 * Created by Rabinovi on 11/12/2016.
 */

public class SuggestedPerson {
    String personUid;
    Integer score;

    public SuggestedPerson(){
    }

    public SuggestedPerson(String pUid, Integer pScore)
    {
        this.personUid = pUid;
        this.score = pScore;
    }
}
